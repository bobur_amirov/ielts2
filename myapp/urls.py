from django.urls import path
from .views import SignUp,SignIn

urlpatterns = [
    path('login', SignIn.as_view()),
    path('signup', SignUp .as_view()),
]