from django.db import models

# Create your models here.
class Section(models.Model):
    name = models.CharField(max_length=50)

class Price(models.Model):
    price = models.FloatField()
    phone = models.IntegerField()
    plastik_number = models.IntegerField()
    plastik_name = models.CharField(max_length=100)

class Topic(models.Model):
    price_id = models.ForeignKey(Price, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    question_number = models.IntegerField()
    question_type = models.CharField(max_length=100)

class TextToRead(models.Model):
    section_id = models.ForeignKey(Section, on_delete=models.CASCADE)
    text = models.TextField()

class Question(models.Model):
    topic_id = models.ForeignKey(Topic, on_delete=models.CASCADE)
    text = models.TextField()
    marked = models.BooleanField()

class PartQuestion(models.Model):
    question_id = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.TextField()

class Dictionary(models.Model):
    question_id = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.TextField()

class Idea(models.Model):
    question_id = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.TextField()   

class Answer(models.Model):
    question_id = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.TextField()

# class Marked(models.Model):
#     question_id = models.ForeignKey(Question, on_delete=models.CASCADE)

class Information(models.Model):
    question = models.TextField()
    answer = models.TextField()

class User(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone = models.IntegerField()
    email = models.EmailField()
    user_type = models.CharField(max_length=50)

class Payment(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    price_id = models.ForeignKey(Price, on_delete=models.CASCADE)
    paid_sum = models.FloatField()
    paid_date = models.DateField()
