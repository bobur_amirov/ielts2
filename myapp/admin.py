from django.contrib import admin
from .models import Section, Price, Topic, TextToRead, Question, PartQuestion, Dictionary, Idea, Answer, Information, User, Payment
# Register your models here.

admin.site.register(Section)
admin.site.register(Price)
admin.site.register(Topic)
admin.site.register(TextToRead)
admin.site.register(Question)
admin.site.register(PartQuestion)
admin.site.register(Dictionary)
admin.site.register(Idea)
admin.site.register(Answer)
# admin.site.register(Marked)
admin.site.register(Information)
admin.site.register(User)
admin.site.register(Payment)