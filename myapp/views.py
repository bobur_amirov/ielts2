from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
# Create your views here.



class SignUp(APIView):
    def post(self, request):
        username = request.data['username']
        email = request.data['email']
        first_name = request.data['first_name']
        last_name = request.data['last_name']
        password = request.data['password']
        user_check = User.objects.filter(username=username).exists()
        if user_check:
            return Response({'code': 'Bunday user oldindan ruyxatdan utgan'})
        else:
            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            token,create = Token.objects.get_or_create(user = user)
            return Response({"token": str(token)})


class SignIn(APIView):
    def post(self, request):
        parser_classes = JSONParser
        username = request.data['username']
        password = request.data['password']
        if username is None or password is None:
            return Response({'code': 'Username yoki password tuldirmagansiz'})
        user = authenticate(username=username,password=password)
        if not user:
            return Response({'code':'Login yoki parol xato qayta urinib kuring'})
        token,create = Token.objects.get_or_create(user=user)
        return Response({'token': str(token)})